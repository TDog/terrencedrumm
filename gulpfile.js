var gulp = require('gulp');
var smushit = require('gulp-smushit');

gulp.task('default', function () {
    return gulp.src('src/**/*.{jpg,png}')
        .pipe(smushit())
        .pipe(gulp.dest('public'));
});